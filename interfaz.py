from tkinter import *
import time
from lexico import *
from gramaticas import *
from gramaticas import lista_errores as e_l
from parsetab import *

import tkinter as tk

ventana = Tk()
ventana.title("Analizador")
ventana.geometry("1200x650")

# cuadro de texto
cuadro_de_texto1 = Text(ventana)
# ubicacion y tamaño
cuadro_de_texto1.pack(padx=10, side=LEFT)
cuadro_de_texto1.place(x=60, y=40, height=240, width=400)
var = StringVar()
label1 = Label(ventana, textvariable=var, relief=RAISED)
var.set("PROGRAMA 1")
label1.place(x=60, y=10, height=30, width=400)

var2 = StringVar()
label2 = Label(ventana, textvariable=var2, relief=RAISED)
var2.set("PROGRAMA 2")
label2.place(x=60, y=270, height=30, width=400)

cuadro_de_texto2 = Text(ventana)
cuadro_de_texto2.pack(padx=0, side=LEFT)
cuadro_de_texto2.place(x=60, y=300, height=240, width=400)

cuadro_de_resul = Text(ventana, state='disabled')
cuadro_de_resul.pack(padx=10, side=LEFT)
cuadro_de_resul.place(x=700, y=40, height=500, width=400)
var3 = StringVar()
label13=Label(ventana,textvariable=var3, relief=RAISED )
var3.set("RESULTADO")
label13.place(x=700,y=40,height=30,width=400)


# funcion boton analisis sintactico
def promedio(tupla1, tupla2):
    #print("aqui estoy")
    #lista2 = []
    coincidencias = 0
    lista1 = list(tupla1)
    lista2=list(tupla2)
    print(lista1)
    print(lista2)
    #change(tupla1, lista1)
   # change(tupla2, lista2)
    if len(lista1) < len(lista2):
        max = len(lista2)
    elif len(lista1) > len(lista2):
        max = len(lista1)
    else:
        max = len(lista1)
    for i in range(max):
        if(i<len(lista1) and i<len(lista2)):
            if lista2[i] in lista1:
                coincidencias += 1
    return "Porcentaje de posible plagio:" + str(round(coincidencias/ max * 100, 2))+ "%"

def boton_lexico():
    cuadro_de_resul.delete(1.0, END)
    inputValue1 = cuadro_de_texto1.get("1.0", "end-1c")
    inputValue2 = cuadro_de_texto2.get("1.0", "end-1c")
    cadena1 = inputValue1.strip()
    cadena2 = inputValue2.strip()
    lista1 = show_tokens(cadena1)
    lista2 = show_tokens(cadena2)
    cuadro_de_resul.config(state="normal")
    inicio = '\n' + '*' * 40 + '\n' + "ANALISIS LEXICO".center(40, " ") + '\n' + '*' * 40 + '\n'
    cuadro_de_resul.insert('end', inicio)
    cuadro_de_resul.insert('end', "programa 1:\n")
    for i in lista1:
        cuadro_de_resul.insert('end', i + "\n")
    cuadro_de_resul.insert('end', "programa 2:\n")
    for i in lista2:
        cuadro_de_resul.insert('end', i + "\n")
    cuadro_de_resul.configure(state='disabled')

def booton_sintactico():
    inputValue1 = cuadro_de_texto1.get("1.0", "end-1c")
    inputValue2 = cuadro_de_texto2.get("1.0", "end-1c")
    cadena1 = inputValue1.strip()
    cadena2 = inputValue2.strip()
    tupla1 = cargar_yacc(cadena1)
    len1 = len(e_l)
    tupla2 = cargar_yacc(cadena2)
    len2 = len(e_l)
    cuadro_de_resul.config(state="normal")
    inicio = '\n' + '*' * 40 + '\n' + "ANALISIS SINTACTICO".center(40, " ") + '\n' + '*' * 40 + '\n'
    cuadro_de_resul.insert('end', inicio)
    cuadro_de_resul.insert('end', "programa 1:\n")
    cuadro_de_resul.insert('end', str(tupla1) + "\n")
    if len1 > 0:
        cuadro_de_resul.insert('end', "Errores en el programa 1:\n")
        for i in range(len1):
            cuadro_de_resul.insert('end', e_l[i] + "\n")
    cuadro_de_resul.insert('end', "programa 2:\n")
    cuadro_de_resul.insert('end', str(tupla2) + "\n")
    if (len2 - len1) > 0:
        cuadro_de_resul.insert('end', "Errores en el programa 2:\n")
        for i in range(len2 - len1):
            cuadro_de_resul.insert('end', e_l[i + len1] + "\n")
    cuadro_de_resul.configure(state='disabled')

def boton_plagio():
    inputValue1=cuadro_de_texto1.get("1.0","end-1c")
    inputValue2=cuadro_de_texto2.get("1.0","end-1c")
    cadena1 = inputValue1.strip()
    cadena2 = inputValue2.strip()
    tupla1=show_tokens(cadena1)
    tupla2=show_tokens(cadena2)
    resultado = promedio(tupla1,tupla2)
    cuadro_de_resul.config(state="normal")
    contenido='Porcentaje de plagio\n' + '*' * 40 + '\n'+resultado.center(40, " ") +'\n' + '*' * 40 + '\n'
    cuadro_de_resul.insert('end', contenido)
    cuadro_de_resul.configure(state='disabled')

def boton_clean():
    cuadro_de_resul.config(state="normal")
    cuadro_de_resul.delete('1.0', tk.END)
    cuadro_de_resul.configure(state='disabled')

# boton para probar el analisis sintactico
btnSint = Button(ventana, height=2, width=14, text="Análisis sintáctico", bg="yellow"
,command=lambda: booton_sintactico())#esta ultima linea espara implementar la funcion que hara el boton
# command=lambda: retrieve_input() >>> just means do this when i press the button
btnSint.pack(side=TOP, pady=100)

# boton para prbar el analisis lexico
btnLex = Button(ventana, height=2, width=14, text="Análisis léxico", bg="yellow"
 ,command=lambda: boton_lexico())
# command=lambda: retrieve_input() >>> just means do this when i press the button
btnLex.pack(side=TOP, pady=50)

btnPlagio = Button(ventana, height=2, width=14, text="Plagio", bg="yellow",command=lambda: boton_plagio())
# command=lambda: retrieve_input() >>> just means do this when i press the button
btnPlagio.pack(side=TOP, pady=100)

btnClean = Button(ventana, height=2, width=14, text="Limpiar", bg="yellow",command=lambda: boton_clean())
# command=lambda: retrieve_input() >>> just means do this when i press the button
btnClean.pack(side=TOP, pady=100)
btnClean.place(x= 10,y= 550)

# para mostrar la ventana
mainloop()
