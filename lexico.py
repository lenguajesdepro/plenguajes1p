import ply.lex as lex

reservadas = {
    'while': 'WHILE',
    'range': 'RANGE',
    'for': 'FOR',
    'in': 'IN',
    'str': 'STRING',
    'print': 'PRINT',
    'len': 'FLEN',
    'from': 'FROM',
    'import': 'IMPORT',
    'random': 'RANDOM',
    'randint': 'FRANDINT',
    'append' : 'FAPPEND',
    'split' : 'FSPLIT'
}

tokens = ['VARIABLE',
          'ENTERO',
          'IGUAL',
          'FLOTANTE',
          'CADENA',
          'LISTA',
          'CONJUNTO',
          'DIFERENTE',
          'MAS',
          'PARENTESISD',
          'PARENTESISI',
          'COMA',
          'CONDICION',
          'DOSPUNTOS',
          'PUNTO',
          'MENORQUE',
          'DIVIDIR'] + \
         list(reservadas.values())


t_ignore = '\t'
t_IGUAL = r'='
t_MAS= r'\+'
t_DIFERENTE = r'\!\='
t_PARENTESISD = r'\('
t_PARENTESISI = r'\)'
t_COMA = r'\,'
t_DOSPUNTOS = r'\:'
t_PUNTO = r'\.'
t_MENORQUE = r'\<'
t_DIVIDIR = r'\/'

# FUNCIONES BASICAS

def t_VARIABLE(t):
    r"""[a-zA-Z_][a-zA-Z_0-9]*"""
    t.type = reservadas.get(t.value, 'VARIABLE')
    return t


def t_CONDICION(t):
    r'''True|False|-{0,1}[0-9]+<-{0,1}[0-9]+|-{0,1}[0-9]+>-{0,1}[0-9]+|-{0,1}[0-9]+<=-{0,1}[0-9]+|-{0,1}[0-9]+>=-{0,1}[0-9]+|-{0,1}[0-9]+<>-{0,1}[0-9]+'''
    t.value=reservadas.get(t.value,'CONDICION')
    return t


def t_FLOTANTE(t):
    r"""(\d)+(\.)(\d)+"""
    t.value = float(t.value)
    return t


def t_ENTERO (t):
    r"""\d+"""
    t.value = int(t.value)
    return t


def t_CADENA(t):
    r"""(\")( )*.*( )*(\")"""
    t.value = str(t.value)
    return t


def t_LISTA(t):
    r"""(\[)[ ]*((\d|(\"\")( )*.*( )*(\"\")|(\d)+(\.)(\d)|True|False|.*)?(\-\d)?(,)?)*[ ]*(\])"""
    t.value = str(t.value)
    return t


def t_CONJUNTO(t):
    r"""(\{)[ ]*((\d|(\"\")( )*.*( )*(\"\")|(\d)+(\.)(\d)|True|False|\w)?(,)?)*[ ]*(\})"""
    t.value = str(t.value)
    return t

def t_newline(t):
    r'\n+'
    t.lexer.lineno += len(t.value)

def t_error(t):
    print("Caracter Invalido '%s'" % t.value[0])
    t.lexer.skip(1)


lexico=lex.lex()


#PRUEBA LEXICO#

def show_tokens(cadena):
    l=[]
    lexico.input(cadena)
    while True:
        tok = lexico.token()
        if not tok:break
        l.append(tok.type)
    return l