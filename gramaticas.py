import lexico
import ply.yacc as yacc

tokens = lexico.tokens
lista_errores=[]

def p_main(p):
    """ main : cuerpo
    """
    p[0] =(p[1])


def p_cuerpo(p):
    """ cuerpo : for
            | while
            | variable
            | print
            | asignacion
            | asignarConjunto
            | asignacionSplit
            | lappend
            | asignarLista
            | import
                | for cuerpo
                | while cuerpo
                | variable cuerpo
                | print cuerpo
                | asignacion cuerpo
                | asignarConjunto cuerpo
                | asignacionSplit cuerpo
                | lappend cuerpo
                | asignarLista cuerpo
                | import cuerpo
    """
    if (len(p) == 2):
        p[0] = (p[1])
    if (len(p) == 3):
        p[0] = ('SENTENCIA', p[1], p[2])

def p_asignacion(p):
    """ asignacion : variable IGUAL numero
                | variable IGUAL slicing MAS slicing
                | variable IGUAL slicing DIVIDIR slicing
                | variable IGUAL frandit
                | variable IGUAL variable
                | variable IGUAL slicing
    """
    if (len(p) == 6):
        p[0] = ('ASIGNACION',p[1], p[2], p[3], p[4], p[5])
    if (len(p) == 4):
        p[0] = ('ASIGNACION',p[1], p[2], p[3])


def p_asignarConjunto(p):
    """ asignarConjunto : variable IGUAL conjunto """
    p[0] = ('ASIGNACION CONJUNTO',p[1], p[2], p[3])


def p_conjunto(p):
    '''conjunto : CONJUNTO'''
    p[0] = (p[1])


def p_import(p):
    """ import : FROM RANDOM IMPORT FRANDINT
    """
    p[0] = ('IMPORT',p[1], p[2], p[3], p[4])


def p_for(p):
    """for : FOR variable IN variable DOSPUNTOS
            | FOR variable IN RANGE PARENTESISD numero COMA numero PARENTESISI DOSPUNTOS
            | FOR variable IN RANGE PARENTESISD COMA numero PARENTESISI DOSPUNTOS
            | FOR variable IN RANGE PARENTESISD COMA flen PARENTESISI DOSPUNTOS
            | FOR variable IN RANGE PARENTESISD flen COMA flen PARENTESISI DOSPUNTOS
            | FOR variable IN RANGE PARENTESISD numero COMA flen PARENTESISI DOSPUNTOS
            | FOR variable IN RANGE PARENTESISD flen COMA numero PARENTESISI DOSPUNTOS
    """
    if (len(p) == 6):
        p[0] = ('FOR',p[1], p[2], p[3], p[4], p[5])
    if (len(p) == 10):
        p[0] = ('FOR',p[1], p[2], p[3], p[4], p[5], p[6],p[7],p[8],p[9])
    if (len(p) == 11):
        p[0] = ('FOR',p[1], p[2], p[3], p[4], p[5], p[6], p[7], p[8], p[9],p[10])


def p_while(p):
    """ while : WHILE PARENTESISD condicion PARENTESISI DOSPUNTOS
    """
    p[0] = ('WHILE',p[1], p[2], p[3],p[4],p[5])


def p_condicion(p):
    """ condicion : flen MENORQUE variable
                  | variable DIFERENTE numero
                  | CONDICION
    """
    if (len(p) == 4):
        p[0] = ('CONDICION',p[1], p[2], p[3])
    if (len(p) == 2):
        p[0] = ('CONDICION',p[1])


def p_lappend(p):
    '''lappend : variable append'''
    p[0] = ('APPEND',p[1], p[2])


def p_append(p):
    '''append : PUNTO FAPPEND PARENTESISD variable PARENTESISI
                | PUNTO FAPPEND PARENTESISD slicing PARENTESISI'''
    if(len(p)==6):
        p[0] = (p[1], p[2], p[3], p[4],p[5])


def p_print(p):
    '''print : PRINT PARENTESISD cadena PARENTESISI
               | PRINT PARENTESISD slicing PARENTESISI
               | PRINT PARENTESISD variable PARENTESISI
               | PRINT PARENTESISD numero PARENTESISI
               | PRINT PARENTESISD cadena MAS str PARENTESISI
               | PRINT PARENTESISD cadena COMA variable PARENTESISI
    '''
    if (len(p) == 5):
        p[0] = ('PRINT',p[1], p[2], p[3], p[4])
    if (len(p) == 7):
        p[0] = ('PRINT',p[1], p[2], p[3], p[4], p[5], p[6])


def p_slicing(p):
    """ slicing : variable lista
    """
    if(len(p)==3):
        p[0]=(p[1], p[2])


def p_flen(p):
    """ flen : FLEN PARENTESISD variable PARENTESISI
    """
    p[0] = ('LEN',p[1], p[2], p[3], p[4])

def p_asignacionSplit(p):
    '''asignacionSplit : variable IGUAL variable split'''
    p[0] = (p[1], p[2], p[3], p[4])

def p_split(p):
    '''split : PUNTO FSPLIT PARENTESISD cadena PARENTESISI'''
    p[0] = ('SPLIT',p[1], p[2], p[3], p[4], p[5])

def p_frandit(p):
    """ frandit : FRANDINT PARENTESISD numero COMA numero PARENTESISI
    """
    p[0] = (p[1], p[2], p[3], p[4],p[5],p[6])

def p_str(p):
    """ str : STRING PARENTESISD variable PARENTESISI
    """
    p[0] = (p[1],p[2],p[3],p[4])

def p_asignarLista(p):
    '''asignarLista : variable IGUAL lista'''
    p[0] = (p[2],p[1],p[3])

def p_lista(p):
    '''lista : LISTA'''
    p[0] = ('LISTA',p[1])

def p_variable(p):
    """variable : VARIABLE
    """
    p[0]=('VARIABLE',p[1])

def p_numero(p):
    '''numero : ENTERO
             | FLOTANTE'''
    p[0]=('NUMERO',p[1])


def p_cadena(p):
    '''cadena : CADENA'''
    p[0]=('CADENA',p[1])


def p_error(p):
    if p is not None:
        lista_errores.append(p.type)
    else:
        print("No se ingreso nada")

yacc.yacc()

def cargar_yacc(cd):
    result=yacc.parse(cd)
    print(result)
    return result


def p_T(p):
    '''T : T newline
    |
    '''